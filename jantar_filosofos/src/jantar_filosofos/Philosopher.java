package jantar_filosofos;

import java.sql.Time;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Philosopher implements Runnable {
	private int id;
	private Chopstick left, right;
	private boolean hasLeft, hasRight;
	private Random gerador = new Random();
	
	public Philosopher(int id, Chopstick left, Chopstick right) {
		this.id = id;
		this.left = left;
		this.right = right;
		this.hasLeft = false;
		this.hasRight = false;
	}
	
	public void eat(){
		// Come em 2 seg
		System.out.println(this + " Comendo...");
		try {
			Thread.sleep(gerador.nextInt(5) + 1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void think(){
		// Pensa em 5 seg
		System.out.println(this + " Pensando...");
		try {
			Thread.sleep(gerador.nextInt(5) + 1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void takeChopsticks() {
		try {
			left.take(this);
			if(hasLeft)
				right.take(this);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void devolveChopsticks() {
		// Devolvo os dois chopsticks
		if(hasLeft) {
			left.devolve(this);
		}
		if(hasRight) {
			right.devolve(this);
		}
		// Notifico todos os fil�sofos que devolvi os chopsticks
		notifyAll();
	}
	
	public void espera() {
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(!Thread.interrupted()) {
			think();
			// Enquanto n�o tiver com os dois chopsticks em m�os
			while(!(hasLeft && hasRight)) {
				// Tento pegar os dois chopsticks
				this.takeChopsticks();
				// Verifico se consegui pegar os dois chopsticks
				if(hasLeft && hasRight) {
					// Se conseguir pegar, come
					eat();
				}else {
					System.out.println(this + " Entrou espera");
					espera();
				}
			}
			// Ap�s conseguir comer devolve os chopsticks
			this.devolveChopsticks();
		}
	}
	public String toString() {
		return "Fil�sofo " + String.valueOf(id);
	}
	public boolean hasLeft() {
		return hasLeft;
	}
	public boolean hasRight() {
		return hasLeft;
	}
	public void setHasLeft(boolean state) {
		hasLeft = state;
	}
	public void setHasRight(boolean state) {
		hasRight = state;
	}
	public Chopstick getLeft() {
		return left;
	}
	public Chopstick getRight() {
		return right;
	}
}
