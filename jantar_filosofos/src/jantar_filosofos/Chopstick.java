package jantar_filosofos;

import java.util.concurrent.atomic.AtomicBoolean;

public class Chopstick {
	private int id;
	private AtomicBoolean available = new AtomicBoolean(true);
	
	public Chopstick(int id) {
		this.id = id;
	}
	
	public synchronized void take(Philosopher p) throws InterruptedException {
		// Se o chopstick que estou tentando pegar n�o est� dispon�vel
		if(!available.get()) {
			// Verifico se esse chopstick � o esquerdo
			if(!p.getLeft().available.get()) {
				System.out.println(p + " Tentou pegar o " + this + ", mas n�o estava dispon�vel.");
				// Se for o esquerdo, verifico se j� tenho o chopstick direito
				if(p.hasRight()) {
					// Se tenho o direito devolvo ele, j� que n�o vou conseguir pegar o esquerdo
					p.devolveChopsticks();
				}
			}else if(!p.getRight().available.get()){
				System.out.println(p + " Tentou pegar o chopstick direito " + this + ", mas n�o estava dispon�vel.");
				if(p.hasLeft()) {
					p.devolveChopsticks();
				}
			}
			return;
		}
		System.out.println(p + " Pegou o " + this);
		available.set(false);
		// Verifico se o chopstick que est� sendo pego � o esquerdo
		if(p.getLeft() == this) {
			p.setHasLeft(true);
		// Verifico se o chopstick que est� sendo pego � o direito
		}else if(p.getRight() == this) {
			p.setHasRight(true);
		}
		
	}
	public synchronized void devolve(Philosopher p) {
		System.out.println(p + " Devolveu o " + this);
		// Verifico se o chopstick que est� sendo devolvido � o esquerdo
		if(p.getLeft() == this) {
			p.setHasLeft(false);
			// Verifico se o chopstick que est� sendo devolvido � o direito
		}else if(p.getRight() == this) {
			p.setHasRight(false);
		}
		available.set(true);
	}
	public String toString() {
		return "Chopstick " + String.valueOf(id);
	}
	
}
