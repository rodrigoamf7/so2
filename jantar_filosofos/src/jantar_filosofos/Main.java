package jantar_filosofos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		Philosopher[] filosofos = new Philosopher[5];
		Chopstick[] chopsticks = new Chopstick[5];
		
		for(int i = 0; i < 5; i++) {
			chopsticks[i] = new Chopstick(i);
		}
		ExecutorService executor = Executors.newCachedThreadPool();
		
		executor.execute(new Philosopher(0, chopsticks[0], chopsticks[1]));
		executor.execute(new Philosopher(1, chopsticks[1], chopsticks[2]));
		executor.execute(new Philosopher(2, chopsticks[2], chopsticks[3]));
		executor.execute(new Philosopher(3, chopsticks[3], chopsticks[4]));
		executor.execute(new Philosopher(4, chopsticks[4], chopsticks[0]));
		
		executor.shutdown();
	}

}
